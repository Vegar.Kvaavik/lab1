package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
        
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    public String user_choice() {
        while(true) {
            String choice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
            if (validate_input(choice, rpsChoices)) {
                return choice;
            } 
            else {
                System.out.printf("I do not understand %s. Could you try again?\n", choice);
            }

        }
        
    }
    
    public boolean is_winner(String choice1, String choice2) {
        if (choice1.equals("paper")) {
            return choice2.equals("rock");
        } else if (choice1.equals("scissors")) {
            return choice2.equals("paper");
        } else {
            return choice2.equals("scissors");
        }
    }
    /*
    Returns true if input == valid_input
    */
    public boolean validate_input(String input, List<String> valid_input) {
        input = input.toLowerCase();
        return valid_input.contains(input);
    }

    public String random_choice() {
        Random rand = new Random();
        return rpsChoices.get(rand.nextInt(3));

    }

    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    List<String> yesNo = Arrays.asList("y", "n");
    
    public void run() {
        while(true) {
            System.out.printf("Let's play round %d\n", roundCounter);
            String human_choice = user_choice();
            String computer_choice = random_choice();
            String choice_string = String.format("Human chose %s, computer chose %s.", human_choice, computer_choice);

            if (is_winner(human_choice, computer_choice)) {
                System.out.println(choice_string + " Human wins!");
                humanScore ++;
            } else if (is_winner(computer_choice, human_choice)) {
                System.out.println(choice_string + " Computer wins!");
                computerScore ++;
            } else {
                System.out.println(choice_string + " It's a tie!");
            }
            System.out.printf("Score: human %d, computer %d\n", humanScore, computerScore);
            String continue_answer = continue_playing();
            if (continue_answer.equals("n")) {
                break;
            }
            roundCounter ++;
        }
        System.out.println("Bye bye :)");
        
    }

    private String continue_playing() {
        while(true) {
            String continue_answer = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
            if (validate_input(continue_answer, yesNo)) {
                return continue_answer;
            } 
            else {
                System.out.printf("I do not understand %s. Could you try again?\n", continue_answer);
            }

        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
